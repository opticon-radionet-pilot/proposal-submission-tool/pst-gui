/*
 Add more unit labels as and when we need them
 */

export const angularUnits =  [
    {value: 'microarcsec', label: '\u{03bc}as'},
    {value: 'milliarcsec', label: 'mas'},
    {value: 'arcsec', label: 'arcsec'},
    {value: 'arcmin', label: 'arcmin'},
    {value: 'milliradians', label: 'mrad'},
    {value: 'degrees', label: 'deg'}
]

export const frequencyUnits = [
    {value: 'kHz', label: 'kHz'},
    {value: 'MHz', label: 'MHz'},
    {value: 'GHz', label: 'GHz'},
    {value: 'THz', label: 'THz'},
    {value: 'PHz', label: 'PHz'},
    {value: 'EHz', label: 'EHz'}
]

export const sensitivityUnits = [
    {value: 'dB', label: 'db'},
    {value: 'bel', label: 'bel'}
]